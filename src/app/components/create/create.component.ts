import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  title = 'Add User';
  angForm: FormGroup;
  constructor(private userservice: UserService, private fb: FormBuilder) {
    this.createForm();
   }
  createForm() {
    this.angForm = this.fb.group({
      firstName: ['', Validators.required ],
      lastName: ['', Validators.required ],
      email: ['', Validators.required ]
   });
  }
  addUser(firstName, lastName, email) {
      this.userservice.addUser(firstName, lastName, email);
  }
  ngOnInit() {
  }
}