import { UserService } from './../../user.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {Sort} from '@angular/material';
import {PageEvent} from '@angular/material';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  allUsers: any;
  users: any;
  pageEvent: PageEvent;
  length = 100;
  pageSize = 10;
  pageSizeOptions = [5, 10, 25, 100];
  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  constructor(private http: HttpClient, private service: UserService) {}

  paginateUsers(event?:PageEvent) {
  	console.log(event.pageIndex);
  	this.pageSize = event.pageSize;	
  	this.users = this.allUsers.slice((event.pageIndex) * event.pageSize, (event.pageIndex + 1) * event.pageSize);
  }

  sortData(sort: Sort) {
    const data = this.allUsers.slice();
    if (!sort.active || sort.direction == '') {
      this.allUsers = data;
      this.users = this.allUsers.slice(0,this.pageSize);
      this.length = this.allUsers.length;
      return;
    }

    this.allUsers = data.sort((a, b) => {
      let isAsc = sort.direction == 'asc';
      switch (sort.active) {
        case 'firstName': return compare(a.firstName, b.firstName, isAsc);
        case 'lastName': return compare(a.lastName, b.lastName, isAsc);
        case 'email': return compare(a.email, b.email, isAsc);
        default: return 0;
      }
    });

    this.users = this.allUsers.slice(0,this.pageSize);
    this.length = this.allUsers.length;
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
  	console.log('get');
    this.service.getUsers().subscribe(res => {
      this.allUsers = res;
      this.users = this.allUsers.slice(0,this.pageSize);
      this.length = this.allUsers.length;
    });
  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}