import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  result: any;
  constructor(private http: HttpClient) {}

  addUser(firstName, lastName, email) {
    const uri = 'http://localhost:58271/api/users/';
    const obj = {
      FirstName: firstName,
      LastName: lastName,
      Email: email
    };
    this.http.post(uri, obj)
        .subscribe(res => console.log('Done'));
  }

  getUsers() {
    const uri = 'http://localhost:58271/api/users/';
    console.log(uri);
    return this
            .http
            .get(uri)
            .map(res => {
            	console.log(res);
              	return res;
            });
  }

  editUser(id) {
    const uri = 'http://localhost:58271/api/users/' + id;
    return this
            .http
            .get(uri)
            .map(res => {
              return res;
            });
  }

  updateUser(firstName, lastName, email, id) {
    const uri = 'http://localhost:58271/api/users/' + id;

    const obj = {
      firstName: firstName,
      lastName: lastName, 
      email: email
    };
    this
      .http
      .put(uri, obj)
      .subscribe(res => console.log('Done'));
  }

  deleteUser(id) {
    const uri = 'http://localhost:58271/api/users/' + id;

        return this
            .http
            .delete(uri)
            .map(res => {
              return res;
            });
  }

}